package ru.pisarev.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.pisarev.tm.endpoint.SessionRecord;

public interface SessionLocator {

    @Nullable
    SessionRecord getSession();

    void setSession(SessionRecord session);
}
