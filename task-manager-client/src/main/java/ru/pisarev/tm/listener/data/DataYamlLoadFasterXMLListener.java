package ru.pisarev.tm.listener.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.pisarev.tm.endpoint.DataEndpoint;
import ru.pisarev.tm.event.ConsoleEvent;
import ru.pisarev.tm.listener.AuthAbstractListener;

@Component
public class DataYamlLoadFasterXMLListener extends AuthAbstractListener {

    @NotNull
    @Autowired
    private DataEndpoint dataEndpoint;

    @Nullable
    public String name() {
        return "data-load-yaml";
    }

    @Nullable
    public String arg() {
        return null;
    }

    @Nullable
    public String description() {
        return "Load data from JSON by FasterXML.";
    }

    @EventListener(condition = "@dataYamlLoadFasterXMLListener.name() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        dataEndpoint.loadDataYaml(getSession());
    }

}