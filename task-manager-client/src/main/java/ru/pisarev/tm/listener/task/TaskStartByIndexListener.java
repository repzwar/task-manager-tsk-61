package ru.pisarev.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.pisarev.tm.endpoint.TaskRecord;
import ru.pisarev.tm.endpoint.TaskEndpoint;
import ru.pisarev.tm.event.ConsoleEvent;
import ru.pisarev.tm.exception.entity.TaskNotFoundException;
import ru.pisarev.tm.listener.TaskAbstractListener;
import ru.pisarev.tm.util.TerminalUtil;

@Component
public class TaskStartByIndexListener extends TaskAbstractListener {

    @NotNull
    @Autowired
    private TaskEndpoint taskEndpoint;

    @Override
    public String name() {
        return "task-start-by-index";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Start task by index.";
    }

    @Override
    @EventListener(condition = "@taskStartByIndexListener.name() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("Enter index");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        @Nullable final TaskRecord task = taskEndpoint.startTaskByIndex(getSession(), index);
        if (task == null) throw new TaskNotFoundException();
    }

}
