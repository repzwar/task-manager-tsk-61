package ru.pisarev.tm.service.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.pisarev.tm.repository.dto.IProjectRecordRepository;
import ru.pisarev.tm.api.service.dto.IProjectRecordService;
import ru.pisarev.tm.dto.ProjectRecord;
import ru.pisarev.tm.enumerated.Status;
import ru.pisarev.tm.exception.empty.EmptyIdException;
import ru.pisarev.tm.exception.empty.EmptyIndexException;
import ru.pisarev.tm.exception.empty.EmptyNameException;
import ru.pisarev.tm.exception.entity.ProjectNotFoundException;
import ru.pisarev.tm.exception.system.IndexIncorrectException;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class ProjectRecordService extends AbstractRecordService<ProjectRecord> implements IProjectRecordService {

    @NotNull
    @Autowired
    private IProjectRecordRepository repository;

    @NotNull
    @Override
    @SneakyThrows
    public List<ProjectRecord> findAll() {
        return repository.findAll();
    }

    @Override
    @SneakyThrows
    public void addAll(@Nullable final Collection<ProjectRecord> collection) {
        if (collection == null) return;
        for (ProjectRecord item : collection) {
            add(item);
        }
    }

    @Nullable
    @Override
    public ProjectRecord add(@Nullable final ProjectRecord entity) {
        if (entity == null) return null;
        repository.save(entity);
        return entity;
    }

    @Nullable
    @Override
    @SneakyThrows
    public ProjectRecord findById(@Nullable final String id) {
        @NotNull final Optional<String> optionalId = Optional.ofNullable(id);
        return repository.findById(optionalId.orElseThrow(EmptyIdException::new)).orElse(null);
    }

    @Override
    @SneakyThrows
    public void clear() {
        repository.deleteAll();
    }

    @Override
    @SneakyThrows
    public void removeById(@Nullable final String id) {
        @NotNull final Optional<String> optionalId = Optional.ofNullable(id);
        repository.deleteById(optionalId.orElseThrow(EmptyIdException::new));
    }

    @Override
    @SneakyThrows
    public void remove(@Nullable final ProjectRecord entity) {
        if (entity == null) return;
        repository.deleteById(entity.getId());
    }


    @NotNull
    @Override
    @SneakyThrows
    public ProjectRecord findByIndex(@NotNull final String userId, @Nullable final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        if (index > repository.findAllByUserId(userId).size() - 1) throw new IndexIncorrectException();
        return repository.findByIndexAndUserId(userId, index);
    }

    @Nullable
    @Override
    @SneakyThrows
    public ProjectRecord findByName(@NotNull final String userId, @Nullable final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return repository.findByUserIdAndName(userId, name);
    }

    @Override
    @SneakyThrows
    public void removeByName(@NotNull final String userId, @Nullable final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        repository.deleteByUserIdAndName(userId, name);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectRecord updateById
            (@NotNull final String userId, @Nullable final String id,
             @Nullable final String name, @Nullable final String description) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final ProjectRecord project = Optional.ofNullable(repository.findByUserIdAndId(userId, id))
                .orElseThrow(ProjectNotFoundException::new);
        project.setName(name);
        project.setDescription(description);
        repository.save(project);
        return project;
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectRecord updateByIndex
            (@NotNull final String userId, @Nullable final Integer index,
             @Nullable final String name, @Nullable final String description) {
        if (index == null || index < 0) throw new EmptyIndexException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final ProjectRecord project = Optional.ofNullable(repository.findByIndexAndUserId(userId, index))
                .orElseThrow(ProjectNotFoundException::new);
        project.setName(name);
        project.setDescription(description);
        repository.save(project);
        return project;
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectRecord startById(@NotNull final String userId, @Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final ProjectRecord project = Optional.ofNullable(repository.findByUserIdAndId(userId, id))
                .orElseThrow(ProjectNotFoundException::new);
        project.setStatus(Status.IN_PROGRESS);
        project.setStartDate(new Date());
        repository.save(project);
        return project;
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectRecord startByIndex(@NotNull final String userId, @Nullable final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        @NotNull final ProjectRecord project = Optional.ofNullable(repository.findByIndexAndUserId(userId, index))
                .orElseThrow(ProjectNotFoundException::new);
        project.setStatus(Status.IN_PROGRESS);
        project.setStartDate(new Date());
        repository.save(project);
        return project;
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectRecord startByName(@NotNull final String userId, @Nullable final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final ProjectRecord project = Optional.ofNullable(repository.findByUserIdAndName(userId, name))
                .orElseThrow(ProjectNotFoundException::new);
        project.setStatus(Status.IN_PROGRESS);
        project.setStartDate(new Date());
        repository.save(project);
        return project;
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectRecord finishById(@NotNull final String userId, @Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final ProjectRecord project = Optional.ofNullable(repository.findByUserIdAndId(userId, id))
                .orElseThrow(ProjectNotFoundException::new);
        project.setStatus(Status.COMPLETED);
        project.setFinishDate(new Date());
        repository.save(project);
        return project;
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectRecord finishByIndex(@NotNull final String userId, @Nullable final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        @NotNull final ProjectRecord project = Optional.ofNullable(repository.findByIndexAndUserId(userId, index))
                .orElseThrow(ProjectNotFoundException::new);
        project.setStatus(Status.COMPLETED);
        project.setFinishDate(new Date());
        repository.save(project);
        return project;
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectRecord finishByName(@NotNull final String userId, @Nullable final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final ProjectRecord project = Optional.ofNullable(repository.findByUserIdAndName(userId, name))
                .orElseThrow(ProjectNotFoundException::new);
        project.setStatus(Status.COMPLETED);
        project.setFinishDate(new Date());
        repository.save(project);
        return project;
    }

    @Nullable
    public ProjectRecord add(String userId, @Nullable String name, @Nullable String description) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final ProjectRecord project = new ProjectRecord(name, description);
        project.setUserId(userId);
        return add(project);
    }

    @NotNull
    @Override
    public List<ProjectRecord> findAll(@NotNull final String userId) {
        return repository.findAllByUserId(userId);
    }

    @Override
    @SneakyThrows
    public void addAll(final String userId, @Nullable final Collection<ProjectRecord> collection) {
        if (collection == null || collection.isEmpty()) return;
        for (ProjectRecord item : collection) {
            item.setUserId(userId);
            add(item);
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public ProjectRecord add(final String userId, @Nullable final ProjectRecord entity) {
        if (entity == null) return null;
        entity.setUserId(userId);
        @Nullable final ProjectRecord entityResult = add(entity);
        return entityResult;
    }

    @Nullable
    @Override
    @SneakyThrows
    public ProjectRecord findById(@NotNull final String userId, @Nullable final String id) {
        @NotNull final Optional<String> optionalId = Optional.ofNullable(id);
        return repository.findByUserIdAndId(userId, optionalId.orElseThrow(EmptyIdException::new));
    }

    @Override
    @SneakyThrows
    public void clear(@NotNull final String userId) {
        repository.deleteByUserId(userId);
    }

    @Override
    @SneakyThrows
    public void removeById(@NotNull final String userId, @Nullable final String id) {
        @NotNull final Optional<String> optionalId = Optional.ofNullable(id);
        repository.deleteByUserIdAndId(userId, optionalId.orElseThrow(EmptyIdException::new));
    }

    @Override
    @SneakyThrows
    public void remove(@NotNull final String userId, @Nullable final ProjectRecord entity) {
        if (entity == null) return;
        repository.deleteByUserIdAndId(userId, entity.getId());
    }

}
