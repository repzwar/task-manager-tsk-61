package ru.pisarev.tm.service;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import ru.pisarev.tm.api.IPropertyService;
import ru.pisarev.tm.api.other.ISignatureSetting;

@Getter
@Service
@PropertySource("classpath:application.properties")
public class PropertyService implements IPropertyService, ISignatureSetting {

    @NotNull
    @Value("#{environment['secret']}")
    public String passwordSecret;

    @NotNull
    @Value("#{environment['iteration']}")
    public Integer passwordIteration;

    @NotNull
    @Value("#{environment['version']}")
    public String applicationVersion;

    @NotNull
    @Value("#{environment['host']}")
    public String serverHost;

    @NotNull
    @Value("#{environment['port']}")
    public String serverPort;

    @NotNull
    @Value("#{environment['sign.secret']}")
    public String signatureSecret;

    @NotNull
    @Value("#{environment['sign.iteration']}")
    public Integer signatureIteration;

    @Nullable
    @Value("#{environment['jdbc.user']}")
    public String jdbcUser;

    @Nullable
    @Value("#{environment['jdbc.password']}")
    public String jdbcPassword;

    @Nullable
    @Value("#{environment['jdbc.url']}")
    public String jdbcUrl;

    @Nullable
    @Value("#{environment['jdbc.driver']}")
    public String jdbcDriver;

    @Nullable
    @Value("#{environment['hibernate.dialect']}")
    public String hibernateDialect;

    @Nullable
    @Value("#{environment['hibernate.hbm2ddl_auto']}")
    public String hibernateBM2DDLAuto;

    @Nullable
    @Value("#{environment['hibernate.show_sql']}")
    public String hibernateShowSql;

    @Nullable
    @Value("#{environment['hibernate.cache.use_second_level_cache']}")
    public String secondLevelCash;

    @Nullable
    @Value("#{environment['hibernate.cache.use_query_cache']}")
    public String queryCache;

    @Nullable
    @Value("#{environment['hibernate.cache.use_minimal_puts']}")
    public String minimalPuts;

    @Nullable
    @Value("#{environment['hibernate.cache.hazelcast.use_lite_member']}")
    public String liteMember;

    @Nullable
    @Value("#{environment['hibernate.cache.region_prefix']}")
    public String regionPrefix;

    @Nullable
    @Value("#{environment['hibernate.cache.provider_configuration_file_resource_path']}")
    public String cacheProvider;

    @Nullable
    @Value("#{environment['hibernate.cache.region.factory_class']}")
    public String factoryClass;

}
