package ru.pisarev.tm.repository.dto;

import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.JpaRepository;
import ru.pisarev.tm.dto.SessionRecord;

import java.util.List;

public interface ISessionRecordRepository extends JpaRepository<SessionRecord, String> {

    List<SessionRecord> findAllByUserId(@Nullable String userId);

    void deleteByUserId(@Nullable String userId);

}
