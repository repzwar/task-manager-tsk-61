package ru.pisarev.tm.repository.model;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.pisarev.tm.model.ProjectGraph;

import java.util.List;

public interface IProjectRepository extends JpaRepository<ProjectGraph, String> {

    ProjectGraph findByUserIdAndId(final String userId, final String id);

    void deleteByUserId(final String userId);

    void deleteByUserIdAndId(final String userId, final String id);

    List<ProjectGraph> findAllByUserId(final String userId);

    ProjectGraph findByUserIdAndName(final String userId, final String name);

    void deleteByUserIdAndName(final String userId, final String name);

}
