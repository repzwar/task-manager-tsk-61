package ru.pisarev.tm.api;


import org.jetbrains.annotations.Nullable;
import ru.pisarev.tm.dto.AbstractRecord;

import java.util.Collection;
import java.util.List;

public interface IRecordService<E extends AbstractRecord> {

    List<E> findAll();

    E add(final E entity);

    void addAll(final Collection<E> collection);

    E findById(final String id);

    void clear();

    void removeById(final String id);

    void remove(final E entity);

}