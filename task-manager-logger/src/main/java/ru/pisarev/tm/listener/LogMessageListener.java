package ru.pisarev.tm.listener;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.pisarev.tm.api.service.ILoggingService;
import ru.pisarev.tm.dto.LoggerDTO;

import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;
import java.io.Serializable;

@Component
@NoArgsConstructor
public class LogMessageListener implements MessageListener {

    @NotNull
    @Autowired
    ILoggingService loggingService;

    @Override
    @SneakyThrows
    public void onMessage(Message message) {
        if (message instanceof ObjectMessage) {
            @NotNull final Serializable entity = ((ObjectMessage) message).getObject();
            if (entity instanceof LoggerDTO)
                loggingService.writeLog((LoggerDTO) entity);
        }
    }

}
